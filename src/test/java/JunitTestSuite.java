/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import service.Client.*;
import service.DeliveryService.*;
import service.ShopItem.*;
import service.Transaction.*;
@RunWith(Suite.class)
@Suite.SuiteClasses({
    ClientServiceDeleteUserTest.class,
    ClientServiceLoginTest.class,
    ClientServiceRegisterTest.class,
    ClientServiceUpdateInfoTest.class,
    DeliveryServiceDeleteDeliveryServiceServiceTest.class,
    DeliveryServiceRegisterServiceTest.class,
    DeliveryServiceUpdateInfoServiceTest.class,
    ShopItemBuyServiceTest.class,
    ShopItemCheckIfPopularServiceTest.class,
    ShopItemGetTrendingIndexServiceTest.class,
    ShopItemPostItemServiceTest.class,
    ShopItemRemoveItemServiceTest.class,
    ShopItemStockUpServiceTest.class,
    TransactionCalculatePriceServiceTest.class,
    TransactionCompleteTransactionServiceTest.class,
    TransactionGetRecentTransactionServiceTest.class
    
    
})

/**
 *
 * @author minag
 */
public class JunitTestSuite {
    
}
