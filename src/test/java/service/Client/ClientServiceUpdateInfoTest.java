/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.Client;

import db.ClientDAO;
import db.DatabaseConnection;
import java.util.ArrayList;
import model.Client;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import service.ClientService;
import static org.junit.Assert.*;

/**
 *
 * @author minag
 */
public class ClientServiceUpdateInfoTest {
    private static ClientDAO clientDAO;
    private static ClientService instance;
    ArrayList<Client> korisnici;
    
    public ClientServiceUpdateInfoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        DatabaseConnection.getConnection();
        ClientServiceUpdateInfoTest.clientDAO = new ClientDAO();
        ClientServiceUpdateInfoTest.instance = new ClientService();
        
        Client korisnik1 = new Client();
        korisnik1.setName("Mina");
        korisnik1.setUsername("mina");
        korisnik1.setPassword("mina");
        
        
        boolean expResult = true;
        boolean result = ClientServiceUpdateInfoTest.clientDAO.insertOne(korisnik1);
        assertEquals(expResult, result);
        
        Client korisnik2 = new Client();
        korisnik2.setName("Marko");
        korisnik2.setUsername("marko");
        korisnik2.setPassword("marko");
        
        expResult = true;
        result = ClientServiceUpdateInfoTest.clientDAO.insertOne(korisnik2);
        assertEquals(expResult, result);
        korisnici = ClientServiceUpdateInfoTest.clientDAO.getAll();
    }
    
    @After
    public void tearDown() {
        DatabaseConnection.close();     
        ArrayList<Client> korisnici = ClientServiceUpdateInfoTest.clientDAO.getAll();
        korisnici.forEach((c) -> {
            ClientServiceUpdateInfoTest.clientDAO.deleteOne(c.getId());
        });
        ClientServiceUpdateInfoTest.clientDAO = null;
        ClientServiceUpdateInfoTest.instance = null;
        korisnici = null;
    }

    /**
     * Test of updateInfo method, of class ClientService.
     */
    @Test
    public void testUpdateInfo() {
        System.out.println("UpdateInfo sa svim praznim podacima, treba da ostanu isti podaci");
        Client c = korisnici.get(0);
        String name = "";
        String oldPassword = "";
        String password = "";
        instance.updateInfo(c, name, oldPassword, password);
        korisnici = ClientServiceUpdateInfoTest.clientDAO.getAll();
        assertEquals(c.getName(), korisnici.get(0).getName());
        assertEquals(c.getPassword(), korisnici.get(0).getPassword());
        assertEquals(c.getUsername(), korisnici.get(0).getUsername());
    }
    
    @Test
    public void testUpdateInfo2() {
        System.out.println("UpdateInfo novim imenom, a praznim poljima za staru i novu lozinku, treba da ostanu isti podaci");
        Client c = korisnici.get(0);
        String name = "Maja";
        String oldPassword = "";
        String password = "";
        instance.updateInfo(c, name, oldPassword, password);
        korisnici = ClientServiceUpdateInfoTest.clientDAO.getAll();
        assertEquals(c.getName(), korisnici.get(0).getName());
        assertEquals(c.getPassword(), korisnici.get(0).getPassword());
        assertEquals(c.getUsername(), korisnici.get(0).getUsername());
    }
    
    @Test
    public void testUpdateInfo3() {
        System.out.println("UpdateInfo starom lozinkom, a praznim poljima za ime i novu lozinku, treba da ostanu isti podaci");
        Client c = korisnici.get(0);
        String name = "";
        String oldPassword = c.getPassword();
        String password = "";
        instance.updateInfo(c, name, oldPassword, password);
        korisnici = ClientServiceUpdateInfoTest.clientDAO.getAll();
        assertEquals(c.getName(), korisnici.get(0).getName());
        assertEquals(c.getPassword(), korisnici.get(0).getPassword());
        assertEquals(c.getUsername(), korisnici.get(0).getUsername());
    }
    
    @Test
    public void testUpdateInfo4() {
        System.out.println("UpdateInfo novom lozinkom, a praznim poljima za ime i starom lozinkom, treba da ostanu isti podaci");
        Client c = korisnici.get(0);
        String name = "";
        String oldPassword = "";
        String password = "fds";
        instance.updateInfo(c, name, oldPassword, password);
        korisnici = ClientServiceUpdateInfoTest.clientDAO.getAll();
        assertEquals(c.getName(), korisnici.get(0).getName());
        assertEquals(c.getPassword(), korisnici.get(0).getPassword());
        assertEquals(c.getUsername(), korisnici.get(0).getUsername());
    }
    
    @Test
    public void testUpdateInfo5() {
        System.out.println("UpdateInfo novim imenom, dobrom starom lozinkom, i istom novom lozinkom, treba da se promene podaci");
        Client c = korisnici.get(0);
        String name = "Maja";
        String oldPassword = c.getPassword();
        String password = c.getPassword();
        instance.updateInfo(c, name, oldPassword, password);
        korisnici = ClientServiceUpdateInfoTest.clientDAO.getAll();
        assertEquals(c.getName(), name);
        assertEquals(c.getPassword(), korisnici.get(0).getPassword());
        assertEquals(c.getUsername(), korisnici.get(0).getUsername());
    }
    
    @Test
    public void testUpdateInfo6() {
        System.out.println("UpdateInfo istim imenom, dobrom starom lozinkom, i novom lozinkom, treba da se promene podaci");
        Client c = korisnici.get(0);
        String name = c.getName();
        String oldPassword = c.getPassword();
        String password = "maja";
        instance.updateInfo(c, name, oldPassword, password);
        korisnici = ClientServiceUpdateInfoTest.clientDAO.getAll();
        assertEquals(c.getName(), name);
        assertEquals(c.getPassword(), korisnici.get(0).getPassword());
        assertEquals(c.getUsername(), korisnici.get(0).getUsername());
    }
}
