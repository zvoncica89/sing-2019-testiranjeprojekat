/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.Client;

import db.ClientDAO;
import db.DatabaseConnection;
import java.util.ArrayList;
import model.Client;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import service.ClientService;
import static org.junit.Assert.*;

/**
 *
 * @author minag
 */
public class ClientServiceRegisterTest {
    private static ClientDAO clientDAO;
    private static ClientService instance;
    
    public ClientServiceRegisterTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        DatabaseConnection.getConnection();
        ClientServiceRegisterTest.clientDAO = new ClientDAO();
        ClientServiceRegisterTest.instance = new ClientService();
        
        Client korisnik1 = new Client();
        korisnik1.setName("Mina");
        korisnik1.setUsername("mina");
        korisnik1.setPassword("mina");
        
        
        boolean expResult = true;
        boolean result = ClientServiceRegisterTest.clientDAO.insertOne(korisnik1);
        assertEquals(expResult, result);
        
        Client korisnik2 = new Client();
        korisnik2.setName("Marko");
        korisnik2.setUsername("marko");
        korisnik2.setPassword("marko");
        
        expResult = true;
        result = ClientServiceRegisterTest.clientDAO.insertOne(korisnik2);
        assertEquals(expResult, result);
    }
    
    @After
    public void tearDown() {
        DatabaseConnection.close();     
        ArrayList<Client> korisnici = ClientServiceRegisterTest.clientDAO.getAll();
        korisnici.forEach((c) -> {
            ClientServiceRegisterTest.clientDAO.deleteOne(c.getId());
        });
        ClientServiceRegisterTest.clientDAO = null;
        ClientServiceRegisterTest.instance = null;
    }

    /**
     * Test of login method, of class ClientService.
     */

    /**
     * Test of register method, of class ClientService.
     */
    
    
    @Test
    public void testRegister1() {
        System.out.println("Registracija sa praznim imenom, korisnickim imenom i lozinkom");
        String name = "";
        String username = "";
        String password = "";
        boolean expResult = false;
        boolean result = instance.register(name, username, password);
        assertEquals(expResult, result);
    }
    
    //Zbog grafa toka kontrole vidimo da ce doci do upisivanja u bazu, zato cemo proveravati i broj korisnika u bazi umesto samo povratne vrednosti
    
    @Test
    public void testRegister2() {
        System.out.println("Registracija sa praznim imenom, korisnickim imenom i lozinkom, provera broja korisnika");
        String name = "";
        String username = "";
        String password = "";
        ArrayList<Client> korisnici = ClientServiceRegisterTest.clientDAO.getAll();
        int expResult = korisnici.size();
        instance.register(name, username, password);
        int result = ClientServiceRegisterTest.clientDAO.getAll().size();
        assertEquals(expResult, result);
    }
    
    @Test
    public void testRegister3() {
        System.out.println("Registracija sa imenom, postojecim korisnickim imenom i lozinkom");
        String name = "fds";
        String username = "mina";
        String password = "fds";
        boolean expResult = false;
        boolean result = instance.register(name, username, password);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testRegister4() {
        System.out.println("Registracija sa imenom, novim korisnickim imenom i lozinkom");
        String name = "fds";
        String username = "dsa";
        String password = "fds";
        boolean expResult = true;
        boolean result = instance.register(name, username, password);
        assertEquals(expResult, result);
    }
    
    
    
    
}
