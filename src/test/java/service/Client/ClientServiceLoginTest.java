/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.Client;

import db.ClientDAO;
import db.DatabaseConnection;
import java.util.ArrayList;
import model.Client;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import service.ClientService;
import static org.junit.Assert.*;

/**
 *
 * @author minag
 */
public class ClientServiceLoginTest {

    private static ClientDAO clientDAO;
    private static ClientService instance;
    
    public ClientServiceLoginTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        DatabaseConnection.getConnection();
        ClientServiceLoginTest.clientDAO = new ClientDAO();
        ClientServiceLoginTest.instance = new ClientService();
        
        Client korisnik1 = new Client();
        korisnik1.setName("Mina");
        korisnik1.setUsername("mina");
        korisnik1.setPassword("mina");
        
        
        boolean expResult = true;
        boolean result = ClientServiceLoginTest.clientDAO.insertOne(korisnik1);
        assertEquals(expResult, result);
        
        Client korisnik2 = new Client();
        korisnik2.setName("Marko");
        korisnik2.setUsername("marko");
        korisnik2.setPassword("marko");
        
        expResult = true;
        result = ClientServiceLoginTest.clientDAO.insertOne(korisnik2);
        assertEquals(expResult, result);
        
        
    }
    
    @AfterClass
    public static void tearDownClass() {
        DatabaseConnection.close();     
        ArrayList<Client> korisnici = ClientServiceLoginTest.clientDAO.getAll();
        korisnici.forEach((c) -> {
            ClientServiceLoginTest.clientDAO.deleteOne(c.getId());
        });
        ClientServiceLoginTest.clientDAO = null;
        ClientServiceLoginTest.instance = null;
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
        
    }

    /**
     * Test of login method, of class ClientService.
     */
    @Test
    public void testLogin1() {
        System.out.println("Login sa praznim korisnickim imenom i lozinkom");
        String username = "";
        String password = "";        
        Client expResult = null;
        Client result = instance.login(username, password);
        assertEquals(expResult, result);
    }
    @Test
    public void testLogin2() {
        System.out.println("Login sa praznim korisnickim imenom a nepostojecom lozinkom");
        String username = "";
        String password = "gfd";        
        Client expResult = null;
        Client result = instance.login(username, password);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testLogin3() {
        System.out.println("Login nepostojecim korisnickim imenom i praznom lozinkom");
        String username = "nfkdsk";
        String password = "";        
        Client expResult = null;
        Client result = instance.login(username, password);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testLogin4() {
        System.out.println("Login sa nepostojecim korisnickim imenom i nepostojecom lozinkom");
        String username = "nfkdsk";
        String password = "fdsfds";        
        Client expResult = null;
        Client result = instance.login(username, password);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testLogin5() {
        System.out.println("Login postojecim korisnickim imenom i praznom lozinkom");
        String username = "mina";
        String password = "";        
        Client expResult = null;
        Client result = instance.login(username, password);
        assertEquals(expResult, result);
    }
    @Test
    public void testLogin6() {
        System.out.println("Login nepostojecim korisnickim imenom i ispravnom lozinkom");
        String username = "mina2";
        String password = "mina";        
        Client expResult = null;
        Client result = instance.login(username, password);
        assertEquals(expResult, result);
    }
    @Test
    public void testLogin7() {
        System.out.println("Login ispravnim korisnickim imenom i ispravnom lozinkom");
        String username = "mina";
        String password = "mina";        
        Client expResult = null;
        Client result = instance.login(username, password);
        assertNotEquals(expResult, result);
    }
    
    @Test
    public void testLogin8() {
        System.out.println("Login ispravnim korisnickim imenom i neispravnom postojecom lozinkom");
        String username = "mina";
        String password = "marko";        
        Client expResult = null;
        Client result = instance.login(username, password);
        assertEquals(expResult, result);
    }
    
}
