/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.Transaction;

import db.ClientDAO;
import db.DatabaseConnection;
import db.DeliveryServiceDAO;
import db.ShopItemDAO;
import db.TransactionDAO;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import model.Client;
import model.DeliveryService;
import model.ShopItem;
import model.Transaction;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import service.PromotionService;
import service.ShopItemService;
import service.TransactionService;
import static org.junit.Assert.*;
import service.ShopItem.ShopItemGetTrendingIndexServiceTest;

/**
 *
 * @author minag
 */
public class TransactionGetRecentTransactionServiceTest {
    private static ShopItemDAO shopItemDAO;
    private static TransactionService instance;
    private static ShopItemService shopItemService;
    private static ClientDAO clientDao;
    private static TransactionDAO transactionDao;
    private static DeliveryServiceDAO deliveryServiceDAO;
    ArrayList<ShopItem> proizvodi;
    ArrayList<Client> korisnici;
    ArrayList<Transaction> transakcije;
    ArrayList<DeliveryService> dobavljaci;
    
    public TransactionGetRecentTransactionServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        DatabaseConnection.getConnection();
        TransactionGetRecentTransactionServiceTest.shopItemService = new ShopItemService();
        TransactionGetRecentTransactionServiceTest.shopItemDAO = new ShopItemDAO();
        TransactionGetRecentTransactionServiceTest.clientDao = new ClientDAO();
        TransactionGetRecentTransactionServiceTest.transactionDao = new TransactionDAO();
        TransactionGetRecentTransactionServiceTest.deliveryServiceDAO = new DeliveryServiceDAO();
        TransactionGetRecentTransactionServiceTest.instance = new TransactionService(TransactionGetRecentTransactionServiceTest.shopItemService);
        
        ShopItem proizvod1 = new ShopItem(null, "Bananica", 12.0F, 150);
        boolean expResult = true;
        boolean result = TransactionGetRecentTransactionServiceTest.shopItemDAO.insertOne(proizvod1);
        
        ShopItem proizvod2 = new ShopItem(null, "Merlot", 400.0F, 10);
        expResult = true;
        result = TransactionGetRecentTransactionServiceTest.shopItemDAO.insertOne(proizvod2);
        
        ShopItem proizvod3 = new ShopItem(null, "Chardonay", 300.0F, 10);
        expResult = true;
        result = TransactionGetRecentTransactionServiceTest.shopItemDAO.insertOne(proizvod3);
        
        proizvodi = TransactionGetRecentTransactionServiceTest.shopItemDAO.getAll();
        
        Client korisnik = new Client(null, "Mina", "mina", "mina");
        expResult = true;
        result = TransactionGetRecentTransactionServiceTest.clientDao.insertOne(korisnik);
        korisnici = TransactionGetRecentTransactionServiceTest.clientDao.getAll();
        
        DeliveryService dobavljac = new DeliveryService(null, "Stark", 10, 10);
        expResult = true;
        result = TransactionGetRecentTransactionServiceTest.deliveryServiceDAO.insertOne(dobavljac);
        dobavljaci = TransactionGetRecentTransactionServiceTest.deliveryServiceDAO.getAll();
        
        transakcije = new ArrayList<>();
    }
    
    @After
    public void tearDown() {
        DatabaseConnection.close();   
        transakcije = TransactionGetRecentTransactionServiceTest.transactionDao.getAll();
        transakcije.forEach((t) -> {
            TransactionGetRecentTransactionServiceTest.transactionDao.deleteOne(t.getId());
        });
        proizvodi = TransactionGetRecentTransactionServiceTest.shopItemDAO.getAll();
        proizvodi.forEach((d) -> {
            TransactionGetRecentTransactionServiceTest.shopItemDAO.deleteOne(d.getId());
        });
        korisnici = TransactionGetRecentTransactionServiceTest.clientDao.getAll();
        korisnici.forEach((c) -> {
            TransactionGetRecentTransactionServiceTest.clientDao.deleteOne(c.getId());
        });
                
        dobavljaci = TransactionGetRecentTransactionServiceTest.deliveryServiceDAO.getAll();
        dobavljaci.forEach((d) -> {
            TransactionGetRecentTransactionServiceTest.deliveryServiceDAO.deleteOne(d.getId());
        });
        
        TransactionGetRecentTransactionServiceTest.shopItemDAO = null;
        TransactionGetRecentTransactionServiceTest.clientDao = null;
        TransactionGetRecentTransactionServiceTest.transactionDao = null;
        TransactionGetRecentTransactionServiceTest.deliveryServiceDAO = null;
        TransactionGetRecentTransactionServiceTest.instance = null;
        
        
        proizvodi = null;
        korisnici = null;
        transakcije = null;    
        dobavljaci = null;
    }

    /**
     * Test of getRecentTransactions method, of class TransactionService.
     */
    @Test
    public void testGetRecentTransactions() {
        System.out.println("getRecentTransactions za transakciju pre 2 dana");
        LocalDate localDate = LocalDate.now().minusDays(2);
        Date date = Date.valueOf(localDate);
        Transaction t = new Transaction(null, 120.0F, 10, date, korisnici.get(0).getId(), proizvodi.get(0).getId(), dobavljaci.get(0).getId(), 10);
        TransactionGetRecentTransactionServiceTest.transactionDao.insertOne(t);
        int expResult = 1;
        ArrayList<Transaction> sveTransakcije = TransactionGetRecentTransactionServiceTest.instance.getRecentTransactions();
        assertEquals(expResult, sveTransakcije.size());
    }
    
    @Test
    public void testGetRecentTransactions2() {
        System.out.println("getRecentTransactions za transakciju pre 30 dana");
        LocalDate localDate = LocalDate.now().minusDays(30);
        Date date = Date.valueOf(localDate);
        Transaction t = new Transaction(null, 120.0F, 10, date, korisnici.get(0).getId(), proizvodi.get(0).getId(), dobavljaci.get(0).getId(), 10);
        TransactionGetRecentTransactionServiceTest.transactionDao.insertOne(t);
        int expResult = 1;
        ArrayList<Transaction> sveTransakcije = TransactionGetRecentTransactionServiceTest.instance.getRecentTransactions();
        assertEquals(expResult, sveTransakcije.size());
    }
    
    @Test
    public void testGetRecentTransactions3() {
        System.out.println("getRecentTransactions za transakciju pre 50 dana");
        LocalDate localDate = LocalDate.now().minusDays(50);
        Date date = Date.valueOf(localDate);
        Transaction t = new Transaction(null, 120.0F, 10, date, korisnici.get(0).getId(), proizvodi.get(0).getId(), dobavljaci.get(0).getId(), 10);
        TransactionGetRecentTransactionServiceTest.transactionDao.insertOne(t);
        int expResult = 0;
        ArrayList<Transaction> sveTransakcije = TransactionGetRecentTransactionServiceTest.instance.getRecentTransactions();
        assertEquals(expResult, sveTransakcije.size());
    }
    
    @Test
    public void testGetRecentTransactions4() {
        System.out.println("getRecentTransactions ako nema transakcija u bazi uopste");
        int expResult = 0;
        ArrayList<Transaction> sveTransakcije = TransactionGetRecentTransactionServiceTest.instance.getRecentTransactions();
        assertEquals(expResult, sveTransakcije.size());
    }
    
}
