/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.Transaction;

import db.ClientDAO;
import db.DatabaseConnection;
import db.DeliveryServiceDAO;
import db.ShopItemDAO;
import db.TransactionDAO;
import java.util.ArrayList;
import model.Client;
import model.DeliveryService;
import model.ShopItem;
import model.Transaction;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import service.TransactionService;
import static org.junit.Assert.*;
import service.ShopItem.ShopItemGetTrendingIndexServiceTest;
import service.ShopItemService;

/**
 *
 * @author minag
 */
public class TransactionCompleteTransactionServiceTest {
    private static ShopItemDAO shopItemDAO;
    private static TransactionService instance;
    private static ShopItemService shopItemService;
    private static ClientDAO clientDao;
    private static TransactionDAO transactionDao;
    private static DeliveryServiceDAO deliveryServiceDAO;
    ArrayList<ShopItem> proizvodi;
    ArrayList<Client> korisnici;
    ArrayList<Transaction> transakcije;
    ArrayList<DeliveryService> dobavljaci;
    
    public TransactionCompleteTransactionServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        DatabaseConnection.getConnection();
        TransactionCompleteTransactionServiceTest.shopItemService = new ShopItemService();
        TransactionCompleteTransactionServiceTest.shopItemDAO = new ShopItemDAO();
        TransactionCompleteTransactionServiceTest.clientDao = new ClientDAO();
        TransactionCompleteTransactionServiceTest.transactionDao = new TransactionDAO();
        TransactionCompleteTransactionServiceTest.deliveryServiceDAO = new DeliveryServiceDAO();
        TransactionCompleteTransactionServiceTest.instance = new TransactionService(TransactionCompleteTransactionServiceTest.shopItemService);
        
        ShopItem proizvod1 = new ShopItem(null, "Bananica", 12.0F, 150);
        boolean expResult = true;
        boolean result = TransactionCompleteTransactionServiceTest.shopItemDAO.insertOne(proizvod1);
        
        ShopItem proizvod2 = new ShopItem(null, "Merlot", 400.0F, 10);
        expResult = true;
        result = TransactionCompleteTransactionServiceTest.shopItemDAO.insertOne(proizvod2);
        
        ShopItem proizvod3 = new ShopItem(null, "Chardonay", 300.0F, 10);
        expResult = true;
        result = TransactionCompleteTransactionServiceTest.shopItemDAO.insertOne(proizvod3);
        
        proizvodi = TransactionCompleteTransactionServiceTest.shopItemDAO.getAll();
        
        Client korisnik = new Client(null, "Mina", "mina", "mina");
        expResult = true;
        result = TransactionCompleteTransactionServiceTest.clientDao.insertOne(korisnik);
        korisnici = TransactionCompleteTransactionServiceTest.clientDao.getAll();
        
        DeliveryService dobavljac = new DeliveryService(null, "Stark", 10, 10);
        expResult = true;
        result = TransactionCompleteTransactionServiceTest.deliveryServiceDAO.insertOne(dobavljac);
        dobavljaci = TransactionCompleteTransactionServiceTest.deliveryServiceDAO.getAll();
        
        transakcije = new ArrayList<>();
    }
    
    @After
    public void tearDown() {
        DatabaseConnection.close();   
        transakcije = TransactionCompleteTransactionServiceTest.transactionDao.getAll();
        transakcije.forEach((t) -> {
            TransactionCompleteTransactionServiceTest.transactionDao.deleteOne(t.getId());
        });
        proizvodi = TransactionCompleteTransactionServiceTest.shopItemDAO.getAll();
        proizvodi.forEach((d) -> {
            TransactionCompleteTransactionServiceTest.shopItemDAO.deleteOne(d.getId());
        });
        korisnici = TransactionCompleteTransactionServiceTest.clientDao.getAll();
        korisnici.forEach((c) -> {
            TransactionCompleteTransactionServiceTest.clientDao.deleteOne(c.getId());
        });
                
        dobavljaci = TransactionCompleteTransactionServiceTest.deliveryServiceDAO.getAll();
        dobavljaci.forEach((d) -> {
            TransactionCompleteTransactionServiceTest.deliveryServiceDAO.deleteOne(d.getId());
        });
        
        TransactionCompleteTransactionServiceTest.shopItemDAO = null;
        TransactionCompleteTransactionServiceTest.clientDao = null;
        TransactionCompleteTransactionServiceTest.transactionDao = null;
        TransactionCompleteTransactionServiceTest.deliveryServiceDAO = null;
        TransactionCompleteTransactionServiceTest.instance = null;
        
        
        proizvodi = null;
        korisnici = null;
        transakcije = null;    
        dobavljaci = null;
        
    }

    /**
     * Test of completeTransaction method, of class TransactionService.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testCompleteTransaction() {
        System.out.println("completeTransaction sa svim praznim poljima");
        Client c = null;
        DeliveryService d = null;
        ShopItem s = null;
        int amount = 0;
        float distance = 0.0F;
        instance.completeTransaction(c, d, s, amount, distance);
    }
    
    @Test
    public void testCompleteTransaction2() {
        System.out.println("completeTransaction sa novom transakcijom i dobrom kolicinom i dobrom distancom");
        Client c = korisnici.get(0);
        DeliveryService d = dobavljaci.get(0);
        ShopItem s = proizvodi.get(0);
        int amount = 10;
        float distance = 2.0F;
        instance.completeTransaction(c, d, s, amount, distance);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testCompleteTransaction3() {
        System.out.println("completeTransaction sa novom transakcijom i dobrom kolicinom i granicnom distancom");
        Client c = korisnici.get(0);
        DeliveryService d = dobavljaci.get(0);
        ShopItem s = proizvodi.get(0);
        int amount = 10;
        float distance = 0.0F;
        instance.completeTransaction(c, d, s, amount, distance);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testCompleteTransaction4() {
        System.out.println("completeTransaction sa novom transakcijom i dobrom kolicinom i negativnom distancom");
        Client c = korisnici.get(0);
        DeliveryService d = dobavljaci.get(0);
        ShopItem s = proizvodi.get(0);
        int amount = 10;
        float distance = -10.0F;
        instance.completeTransaction(c, d, s, amount, distance);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testCompleteTransaction5() {
        System.out.println("completeTransaction sa novom transakcijom i 0/granicnom kolicinom i dobrom distancom");
        Client c = korisnici.get(0);
        DeliveryService d = dobavljaci.get(0);
        ShopItem s = proizvodi.get(0);
        int amount = 0;
        float distance = 10.0F;
        instance.completeTransaction(c, d, s, amount, distance);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCompleteTransaction6() {
        System.out.println("completeTransaction sa novom transakcijom i negativnom kolicinom i dobrom distancom");
        Client c = korisnici.get(0);
        DeliveryService d = dobavljaci.get(0);
        ShopItem s = proizvodi.get(0);
        int amount = -10;
        float distance = 10.0F;
        instance.completeTransaction(c, d, s, amount, distance);
    }
    
    
}
