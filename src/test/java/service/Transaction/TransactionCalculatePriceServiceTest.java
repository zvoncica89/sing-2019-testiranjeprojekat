/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.Transaction;

import db.ClientDAO;
import db.DatabaseConnection;
import db.DeliveryServiceDAO;
import db.ShopItemDAO;
import db.TransactionDAO;
import java.util.ArrayList;
import java.util.Date;
import model.Client;
import model.DeliveryService;
import model.Promotion;
import model.ShopItem;
import model.Transaction;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import service.TransactionService;
import static org.junit.Assert.*;
import org.mockito.InOrder;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import service.PromotionService;
import service.ShopItemService;

/**
 *
 * @author minag
 */
public class TransactionCalculatePriceServiceTest {
    private static ShopItemDAO shopItemDAO;
    private static TransactionService instance;
    private static ShopItemService shopItemService;
    private static ClientDAO clientDao;
    private static TransactionDAO transactionDao;
    private static DeliveryServiceDAO deliveryServiceDAO;
    ArrayList<ShopItem> proizvodi;
    ArrayList<Client> korisnici;
    ArrayList<Transaction> transakcije;
    ArrayList<DeliveryService> dobavljaci;
    Promotion p;
    
    public TransactionCalculatePriceServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        DatabaseConnection.getConnection();
        TransactionCalculatePriceServiceTest.shopItemDAO = new ShopItemDAO();
        TransactionCalculatePriceServiceTest.clientDao = new ClientDAO();
        TransactionCalculatePriceServiceTest.transactionDao = new TransactionDAO();
        TransactionCalculatePriceServiceTest.deliveryServiceDAO = new DeliveryServiceDAO();
        TransactionCalculatePriceServiceTest.shopItemService = new ShopItemService();
        TransactionCalculatePriceServiceTest.instance = new TransactionService(TransactionCalculatePriceServiceTest.shopItemService);
        
        ShopItem proizvod1 = new ShopItem(null, "Bananica", 12.0F, 150);
        boolean expResult = true;
        boolean result = TransactionCalculatePriceServiceTest.shopItemDAO.insertOne(proizvod1);
        
        ShopItem proizvod2 = new ShopItem(null, "Merlot", 400.0F, 200);
        expResult = true;
        result = TransactionCalculatePriceServiceTest.shopItemDAO.insertOne(proizvod2);
        
        ShopItem proizvod3 = new ShopItem(null, "Chardonay", 100.0F, 200);
        expResult = true;
        result = TransactionCalculatePriceServiceTest.shopItemDAO.insertOne(proizvod3);
        
        proizvodi = TransactionCalculatePriceServiceTest.shopItemDAO.getAll();
        
        Client korisnik = new Client(null, "Mina", "mina", "mina");
        expResult = true;
        result = TransactionCalculatePriceServiceTest.clientDao.insertOne(korisnik);
        korisnici = TransactionCalculatePriceServiceTest.clientDao.getAll();
        
        DeliveryService dobavljac = new DeliveryService(null, "Stark", 1000, 200);
        expResult = true;
        result = TransactionCalculatePriceServiceTest.deliveryServiceDAO.insertOne(dobavljac);
        dobavljaci = TransactionCalculatePriceServiceTest.deliveryServiceDAO.getAll();
        
        transakcije = new ArrayList<>();
        
        
        
    }
    
    @After
    public void tearDown() {
        DatabaseConnection.close();   
        transakcije = TransactionCalculatePriceServiceTest.transactionDao.getAll();
        transakcije.forEach((t) -> {
            TransactionCalculatePriceServiceTest.transactionDao.deleteOne(t.getId());
        });
        proizvodi = TransactionCalculatePriceServiceTest.shopItemDAO.getAll();
        proizvodi.forEach((d) -> {
            TransactionCalculatePriceServiceTest.shopItemDAO.deleteOne(d.getId());
        });
        korisnici = TransactionCalculatePriceServiceTest.clientDao.getAll();
        korisnici.forEach((c) -> {
            TransactionCalculatePriceServiceTest.clientDao.deleteOne(c.getId());
        });
                
        dobavljaci = TransactionCalculatePriceServiceTest.deliveryServiceDAO.getAll();
        dobavljaci.forEach((d) -> {
            TransactionCalculatePriceServiceTest.deliveryServiceDAO.deleteOne(d.getId());
        });
        
        TransactionCalculatePriceServiceTest.shopItemDAO = null;
        TransactionCalculatePriceServiceTest.clientDao = null;
        TransactionCalculatePriceServiceTest.transactionDao = null;
        TransactionCalculatePriceServiceTest.deliveryServiceDAO = null;
        TransactionCalculatePriceServiceTest.instance = null;
        
        
        proizvodi = null;
        korisnici = null;
        transakcije = null;    
        dobavljaci = null;
    }

    
    
    @Test
    public void testCalculatePrice1() {
        System.out.println("CalculatePrice bez praznika");
        ShopItem s = proizvodi.get(2);
        DeliveryService d = dobavljaci.get(0);
        
        float distance = 100; // 100km
        int amount = 100;
        Date now = new Date(System.currentTimeMillis());
        
//        racunanje cene
        float expectedPrice = d.getStartingPrice() + s.getPrice();
        if(amount > 20 && amount < 50){
            expectedPrice = (float) (expectedPrice * 0.9);
        } else if(amount >= 50){
            expectedPrice = (float) (expectedPrice * 0.8);
        }
        expectedPrice -= 350/distance*20;
        expectedPrice += distance*100*d.getPricePerKilometer();

        PromotionService promotionServiceSpy1 = spy(PromotionService.class);

        when(promotionServiceSpy1.checkHolidays(now)).thenReturn(false);
        float discount = promotionServiceSpy1.applyPromotions(expectedPrice, now);
        if(discount > expectedPrice/2){

        } else {
            expectedPrice -= discount;
        }

        PromotionService promotionServiceSpy2 = spy(PromotionService.class);
        TransactionCalculatePriceServiceTest.instance.setPs(promotionServiceSpy2);
        
//        zadamo da nije holiday
        when(promotionServiceSpy2.checkHolidays(now)).thenReturn(false);
        float result = TransactionCalculatePriceServiceTest.instance.calculatePrice(s, distance, amount, d);
        
        assertEquals(expectedPrice, result, 0.0);
        
//        InOrder promotionServiceOrderNoHoliday = inOrder(promotionServiceSpy2);
//        promotionServiceOrderNoHoliday.verify(promotionServiceSpy2, times(1)).calculateSpecialPromotions();
//        promotionServiceOrderNoHoliday.verify(promotionServiceSpy2, times(1)).checkHolidays(now);
//        promotionServiceOrderNoHoliday.verify(promotionServiceSpy2, times(1)).calculateDiscount();
//        promotionServiceOrderNoHoliday.verify(promotionServiceSpy2, never()).calculateSeasonalPromotions(now);
    }
    
    @Test
    public void testCalculatePrice2() {
        System.out.println("CalculatePrice sa praznikom");
        ShopItem s = proizvodi.get(2);
        DeliveryService d = dobavljaci.get(0);
        
        float distance = 100; // 100km
        int amount = 100;
        Date now = new Date(System.currentTimeMillis());
        
//        racunanje cene
        float expectedPrice = d.getStartingPrice() + s.getPrice();
        if(amount > 20 && amount < 50){
            expectedPrice = (float) (expectedPrice * 0.9);
        } else if(amount >= 50){
            expectedPrice = (float) (expectedPrice * 0.8);
        }
        expectedPrice -= 350/distance*20;
        expectedPrice += distance*100*d.getPricePerKilometer();

        PromotionService promotionServiceSpy1 = spy(PromotionService.class);

        when(promotionServiceSpy1.checkHolidays(now)).thenReturn(false);
        float discount = promotionServiceSpy1.applyPromotions(expectedPrice, now);
        if(discount > expectedPrice/2){

        } else {
            expectedPrice -= discount;
        }

        PromotionService promotionServiceSpy2 = spy(PromotionService.class);
        TransactionCalculatePriceServiceTest.instance.setPs(promotionServiceSpy2);
        
        when(promotionServiceSpy2.checkHolidays(now)).thenReturn(true);
        float result = TransactionCalculatePriceServiceTest.instance.calculatePrice(s, distance, amount, d);
        
        assertEquals(expectedPrice, result, 0.0);
        
//        InOrder promotionServiceOrderIsHoliday = inOrder(promotionServiceSpy2);
//        promotionServiceOrderIsHoliday.verify(promotionServiceSpy2).calculateSpecialPromotions();
//        promotionServiceOrderIsHoliday.verify(promotionServiceSpy2).checkHolidays(now);
//        promotionServiceOrderIsHoliday.verify(promotionServiceSpy2).calculateSeasonalPromotions(now);
//        promotionServiceOrderIsHoliday.verify(promotionServiceSpy2).calculateDiscount();

    }
    
    @Test
    public void testCalculatePrice3() {
        System.out.println("CalculatePrice sa nepostojecim ShopItemom");
        ShopItem s = null;
        DeliveryService d = dobavljaci.get(0);
        
        float distance = 100; // 100km
        int amount = 100;
        Date now = new Date(System.currentTimeMillis());
        
//        racunanje cene
        float expectedPrice = d.getStartingPrice() + s.getPrice();
        if(amount > 20 && amount < 50){
            expectedPrice = (float) (expectedPrice * 0.9);
        } else if(amount >= 50){
            expectedPrice = (float) (expectedPrice * 0.8);
        }
        expectedPrice -= 350/distance*20;
        expectedPrice += distance*100*d.getPricePerKilometer();

        PromotionService promotionServiceSpy1 = spy(PromotionService.class);

        when(promotionServiceSpy1.checkHolidays(now)).thenReturn(false);
        float discount = promotionServiceSpy1.applyPromotions(expectedPrice, now);
        if(discount > expectedPrice/2){

        } else {
            expectedPrice -= discount;
        }

        PromotionService promotionServiceSpy2 = spy(PromotionService.class);
        TransactionCalculatePriceServiceTest.instance.setPs(promotionServiceSpy2);
        
        when(promotionServiceSpy2.checkHolidays(now)).thenReturn(true);
        float result = TransactionCalculatePriceServiceTest.instance.calculatePrice(s, distance, amount, d);
        
        assertEquals(expectedPrice, result, 0.0);
        
//        InOrder promotionServiceOrderIsHoliday = inOrder(promotionServiceSpy2);
//        promotionServiceOrderIsHoliday.verify(promotionServiceSpy2).calculateSpecialPromotions();
//        promotionServiceOrderIsHoliday.verify(promotionServiceSpy2).checkHolidays(now);
//        promotionServiceOrderIsHoliday.verify(promotionServiceSpy2).calculateSeasonalPromotions(now);
//        promotionServiceOrderIsHoliday.verify(promotionServiceSpy2).calculateDiscount();

    }
    
    @Test
    public void testCalculatePrice4() {
        System.out.println("CalculatePrice sa nepostojecim DeliveryService-om");
        ShopItem s = proizvodi.get(2);
        DeliveryService d = null;
        
        float distance = 100; // 100km
        int amount = 100;
        Date now = new Date(System.currentTimeMillis());
        
//        racunanje cene
        float expectedPrice = d.getStartingPrice() + s.getPrice();
        if(amount > 20 && amount < 50){
            expectedPrice = (float) (expectedPrice * 0.9);
        } else if(amount >= 50){
            expectedPrice = (float) (expectedPrice * 0.8);
        }
        expectedPrice -= 350/distance*20;
        expectedPrice += distance*100*d.getPricePerKilometer();

        PromotionService promotionServiceSpy1 = spy(PromotionService.class);

        when(promotionServiceSpy1.checkHolidays(now)).thenReturn(false);
        float discount = promotionServiceSpy1.applyPromotions(expectedPrice, now);
        if(discount > expectedPrice/2){

        } else {
            expectedPrice -= discount;
        }

        PromotionService promotionServiceSpy2 = spy(PromotionService.class);
        TransactionCalculatePriceServiceTest.instance.setPs(promotionServiceSpy2);
        
        when(promotionServiceSpy2.checkHolidays(now)).thenReturn(true);
        float result = TransactionCalculatePriceServiceTest.instance.calculatePrice(s, distance, amount, d);
        
        assertEquals(expectedPrice, result, 0.0);
        
        InOrder promotionServiceOrderIsHoliday = inOrder(promotionServiceSpy2);
        promotionServiceOrderIsHoliday.verify(promotionServiceSpy2).calculateSpecialPromotions();
        promotionServiceOrderIsHoliday.verify(promotionServiceSpy2).checkHolidays(now);
        promotionServiceOrderIsHoliday.verify(promotionServiceSpy2).calculateSeasonalPromotions(now);
        promotionServiceOrderIsHoliday.verify(promotionServiceSpy2).calculateDiscount();

    }
    
    @Test
    public void testCalculatePrice5() {
        System.out.println("Provera inordera za praznik");
        ShopItem s = proizvodi.get(2);
        DeliveryService d = null;
        
        float distance = 100; // 100km
        int amount = 100;
        Date now = new Date(System.currentTimeMillis());
        
//        racunanje cene
        float expectedPrice = d.getStartingPrice() + s.getPrice();
        if(amount > 20 && amount < 50){
            expectedPrice = (float) (expectedPrice * 0.9);
        } else if(amount >= 50){
            expectedPrice = (float) (expectedPrice * 0.8);
        }
        expectedPrice -= 350/distance*20;
        expectedPrice += distance*100*d.getPricePerKilometer();

        PromotionService promotionServiceSpy1 = spy(PromotionService.class);

        when(promotionServiceSpy1.checkHolidays(now)).thenReturn(false);
        float discount = promotionServiceSpy1.applyPromotions(expectedPrice, now);
        if(discount > expectedPrice/2){

        } else {
            expectedPrice -= discount;
        }

        PromotionService promotionServiceSpy2 = spy(PromotionService.class);
        TransactionCalculatePriceServiceTest.instance.setPs(promotionServiceSpy2);
        
        when(promotionServiceSpy2.checkHolidays(now)).thenReturn(true);
        float result = TransactionCalculatePriceServiceTest.instance.calculatePrice(s, distance, amount, d);
        
        InOrder promotionServiceOrderIsHoliday = inOrder(promotionServiceSpy2);
        promotionServiceOrderIsHoliday.verify(promotionServiceSpy2).calculateSpecialPromotions();
        promotionServiceOrderIsHoliday.verify(promotionServiceSpy2).checkHolidays(now);
        promotionServiceOrderIsHoliday.verify(promotionServiceSpy2).calculateSeasonalPromotions(now);
        promotionServiceOrderIsHoliday.verify(promotionServiceSpy2).calculateDiscount();

    }
    
    @Test
    public void testCalculatePrice6() {
        System.out.println("Provera inOrder bez praznika");
        ShopItem s = proizvodi.get(2);
        DeliveryService d = dobavljaci.get(0);
        
        float distance = 100; // 100km
        int amount = 100;
        Date now = new Date(System.currentTimeMillis());
        
//        racunanje cene
        float expectedPrice = d.getStartingPrice() + s.getPrice();
        if(amount > 20 && amount < 50){
            expectedPrice = (float) (expectedPrice * 0.9);
        } else if(amount >= 50){
            expectedPrice = (float) (expectedPrice * 0.8);
        }
        expectedPrice -= 350/distance*20;
        expectedPrice += distance*100*d.getPricePerKilometer();

        PromotionService promotionServiceSpy1 = spy(PromotionService.class);

        when(promotionServiceSpy1.checkHolidays(now)).thenReturn(false);
        float discount = promotionServiceSpy1.applyPromotions(expectedPrice, now);
        if(discount > expectedPrice/2){

        } else {
            expectedPrice -= discount;
        }

        PromotionService promotionServiceSpy2 = spy(PromotionService.class);
        TransactionCalculatePriceServiceTest.instance.setPs(promotionServiceSpy2);
        
//        zadamo da nije holiday
        when(promotionServiceSpy2.checkHolidays(now)).thenReturn(false);
        float result = TransactionCalculatePriceServiceTest.instance.calculatePrice(s, distance, amount, d);
        
        InOrder promotionServiceOrderNoHoliday = inOrder(promotionServiceSpy2);
        promotionServiceOrderNoHoliday.verify(promotionServiceSpy2, times(1)).calculateSpecialPromotions();
        promotionServiceOrderNoHoliday.verify(promotionServiceSpy2, times(1)).checkHolidays(now);
        promotionServiceOrderNoHoliday.verify(promotionServiceSpy2, times(1)).calculateDiscount();
        promotionServiceOrderNoHoliday.verify(promotionServiceSpy2, never()).calculateSeasonalPromotions(now);
    }
    

    
}
