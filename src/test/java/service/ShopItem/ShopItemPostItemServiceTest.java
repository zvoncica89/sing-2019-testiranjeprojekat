/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.ShopItem;

import db.DatabaseConnection;
import db.ShopItemDAO;
import java.util.ArrayList;
import model.ShopItem;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import service.ShopItemService;
import static org.junit.Assert.*;

/**
 *
 * @author minag
 */
public class ShopItemPostItemServiceTest {
    private static ShopItemDAO shopItemDAO;
    private static ShopItemService instance;
    ArrayList<ShopItem> proizvodi;
    
    public ShopItemPostItemServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        DatabaseConnection.getConnection();
        ShopItemPostItemServiceTest.shopItemDAO = new ShopItemDAO();
        ShopItemPostItemServiceTest.instance = new ShopItemService();
        
        ShopItem proizvod1 = new ShopItem(null, "Bananica", 12.0F, 150);
        boolean expResult = true;
        boolean result = ShopItemPostItemServiceTest.shopItemDAO.insertOne(proizvod1);
        
        ShopItem proizvod2 = new ShopItem(null, "Coca-Cola", 120.0F, 10);
        expResult = true;
        result = ShopItemPostItemServiceTest.shopItemDAO.insertOne(proizvod2);
        
        proizvodi = ShopItemPostItemServiceTest.shopItemDAO.getAll();
    }
    
    @After
    public void tearDown() {
        DatabaseConnection.close();     
        proizvodi.forEach((d) -> {
            ShopItemPostItemServiceTest.shopItemDAO.deleteOne(d.getId());
        });
        ShopItemPostItemServiceTest.shopItemDAO = null;
        ShopItemPostItemServiceTest.instance = null;
        proizvodi = null;
    }

    /**
     * Test of postItem method, of class ShopItemService.
     */
    
    /** price:
     * legalna klasa: price >= 0;
     * nelegalna klasa: price < 0;
     * 
     * amount:
     * legalna klasa: amount >= 0;
     * nelegalna klasa: amount < 0;
    */
    @Test
    public void testPostItem1() {
        System.out.println("PostItem sa praznim imenom, sa cenom < 0, kolicinom < 0");
        String name = "";
        float price = -10.0F;
        int amount = -10;
        boolean expResult = false;
        boolean result = instance.postItem(name, price, amount);
        assertEquals(expResult, result);
    }
    @Test
    public void testPostItem11() {
        System.out.println("PostItem sa imenom, sa cenom < 0, kolicinom < 0");
        String name = "Novo ime";
        float price = -10.0F;
        int amount = -10;
        boolean expResult = false;
        boolean result = instance.postItem(name, price, amount);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testPostItem12() {
        System.out.println("PostItem sa praznim imenom, sa cenom < 0, kolicinom = 0");
        String name = "";
        float price = -10.0F;
        int amount = 0;
        boolean expResult = false;
        boolean result = instance.postItem(name, price, amount);
        assertEquals(expResult, result);
    }
    @Test
    public void testPostItem121() {
        System.out.println("PostItem sa imenom, sa cenom < 0, kolicinom = 0");
        String name = "Novo ime";
        float price = -10.0F;
        int amount = 0;
        boolean expResult = false;
        boolean result = instance.postItem(name, price, amount);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testPostItem13() {
        System.out.println("PostItem sa praznim imenom, sa cenom < 0, kolicinom > 0");
        String name = "";
        float price = -10.0F;
        int amount = 10;
        boolean expResult = false;
        boolean result = instance.postItem(name, price, amount);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testPostItem131() {
        System.out.println("PostItem sa imenom, sa cenom < 0, kolicinom > 0");
        String name = "Novo ime";
        float price = -10.0F;
        int amount = 10;
        boolean expResult = false;
        boolean result = instance.postItem(name, price, amount);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testPostItem14() {
        System.out.println("PostItem sa praznim imenom, sa cenom = 0, kolicinom < 0");
        String name = "";
        float price = 0.0F;
        int amount = -10;
        boolean expResult = false;
        boolean result = instance.postItem(name, price, amount);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testPostItem141() {
        System.out.println("PostItem sa imenom, sa cenom = 0, kolicinom < 0");
        String name = "Novo ime";
        float price = 0.0F;
        int amount = -10;
        boolean expResult = false;
        boolean result = instance.postItem(name, price, amount);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testPostItem15() {
        System.out.println("PostItem sa praznim imenom, sa cenom > 0, kolicinom < 0");
        String name = "";
        float price = 10.0F;
        int amount = -10;
        boolean expResult = false;
        boolean result = instance.postItem(name, price, amount);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testPostItem151() {
        System.out.println("PostItem sa  imenom, sa cenom > 0, kolicinom < 0");
        String name = "Novo ime";
        float price = 10.0F;
        int amount = -10;
        boolean expResult = false;
        boolean result = instance.postItem(name, price, amount);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testPostItem2() {
        System.out.println("PostItem sa imenom, sa cenom = 0, kolicinom = 0");
        String name = "Novi proizvod";
        float price = 0.0F;
        int amount = 0;
        boolean expResult = true;
        boolean result = instance.postItem(name, price, amount);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testPostItem21() {
        System.out.println("PostItem sa imenom, sa cenom > 0, kolicinom = 0");
        String name = "Novi proizvod";
        float price = 10.0F;
        int amount = 0;
        boolean expResult = true;
        boolean result = instance.postItem(name, price, amount);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testPostItem22() {
        System.out.println("PostItem sa imenom, sa cenom = 0, kolicinom > 0");
        String name = "Novi proizvod";
        float price = 0.0F;
        int amount = 10;
        boolean expResult = true;
        boolean result = instance.postItem(name, price, amount);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testPostItem23() {
        System.out.println("PostItem sa imenom, sa cenom > 0, kolicinom > 0");
        String name = "Novi proizvod";
        float price = 10.0F;
        int amount = 10;
        boolean expResult = true;
        boolean result = instance.postItem(name, price, amount);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testPostItem3() {
        System.out.println("PostItem sa postojecim imenom, sa cenom > 0, kolicinom > 0");
        String name = "Bananica";
        float price = 10.0F;
        int amount = 10;
        boolean expResult = true;
        boolean result = instance.postItem(name, price, amount);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testPostItem4() {
        System.out.println("PostItem sa postojecim imenom, sa cenom > 0, kolicinom > 0, proverom da li postoje dva proizvoda sa razlicitim id-em");
        String name = "Bananica";
        float price = 10.0F;
        int amount = 10;
        instance.postItem(name, price, amount);
        proizvodi = ShopItemPostItemServiceTest.shopItemDAO.getAll();
        ArrayList<ShopItem> sviIstiProizvodi = new ArrayList<>();
        for (ShopItem proizvod : proizvodi){
            if(proizvod.getName().equals("Bananica")){
                sviIstiProizvodi.add(proizvod);
            }
        }
        assertNotEquals(sviIstiProizvodi.get(0).getId(), sviIstiProizvodi.get(1).getId());
        
    }
   
    
}
