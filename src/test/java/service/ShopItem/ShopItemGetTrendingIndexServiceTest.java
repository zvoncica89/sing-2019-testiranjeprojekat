/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.ShopItem;

import db.ClientDAO;
import db.DatabaseConnection;
import db.DeliveryServiceDAO;
import db.ShopItemDAO;
import db.TransactionDAO;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import model.Client;
import model.DeliveryService;
import model.ShopItem;
import model.Transaction;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import service.ShopItemService;
import static org.junit.Assert.*;

/**
 *
 * @author minag
 */
public class ShopItemGetTrendingIndexServiceTest {
    private static ShopItemDAO shopItemDAO;
    private static ShopItemService instance;
    private static ClientDAO clientDao;
    private static TransactionDAO transactionDao;
    private static DeliveryServiceDAO deliveryServiceDAO;
    ArrayList<ShopItem> proizvodi;
    ArrayList<Client> korisnici;
    ArrayList<Transaction> transakcije;
    ArrayList<DeliveryService> dobavljaci;
    
    public ShopItemGetTrendingIndexServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        DatabaseConnection.getConnection();
        ShopItemGetTrendingIndexServiceTest.shopItemDAO = new ShopItemDAO();
        ShopItemGetTrendingIndexServiceTest.clientDao = new ClientDAO();
        ShopItemGetTrendingIndexServiceTest.transactionDao = new TransactionDAO();
        ShopItemGetTrendingIndexServiceTest.deliveryServiceDAO = new DeliveryServiceDAO();
        ShopItemGetTrendingIndexServiceTest.instance = new ShopItemService();
        
        ShopItem proizvod1 = new ShopItem(null, "Bananica", 12.0F, 150);
        boolean expResult = true;
        boolean result = ShopItemGetTrendingIndexServiceTest.shopItemDAO.insertOne(proizvod1);
        
        ShopItem proizvod2 = new ShopItem(null, "Merlot", 400.0F, 10);
        expResult = true;
        result = ShopItemGetTrendingIndexServiceTest.shopItemDAO.insertOne(proizvod2);
        
        ShopItem proizvod3 = new ShopItem(null, "Chardonay", 300.0F, 10);
        expResult = true;
        result = ShopItemGetTrendingIndexServiceTest.shopItemDAO.insertOne(proizvod3);
        
        proizvodi = ShopItemGetTrendingIndexServiceTest.shopItemDAO.getAll();
        
        Client korisnik = new Client(null, "Mina", "mina", "mina");
        expResult = true;
        result = ShopItemGetTrendingIndexServiceTest.clientDao.insertOne(korisnik);
        korisnici = ShopItemGetTrendingIndexServiceTest.clientDao.getAll();
        
        DeliveryService dobavljac = new DeliveryService(null, "Stark", 10, 10);
        expResult = true;
        result = ShopItemGetTrendingIndexServiceTest.deliveryServiceDAO.insertOne(dobavljac);
        dobavljaci = ShopItemGetTrendingIndexServiceTest.deliveryServiceDAO.getAll();
        
        transakcije = new ArrayList<>();
    }
    
    @After
    public void tearDown() {
        DatabaseConnection.close();   
        transakcije = ShopItemGetTrendingIndexServiceTest.transactionDao.getAll();
        transakcije.forEach((t) -> {
            ShopItemGetTrendingIndexServiceTest.transactionDao.deleteOne(t.getId());
        });
        proizvodi = ShopItemGetTrendingIndexServiceTest.shopItemDAO.getAll();
        proizvodi.forEach((d) -> {
            ShopItemGetTrendingIndexServiceTest.shopItemDAO.deleteOne(d.getId());
        });
        korisnici = ShopItemGetTrendingIndexServiceTest.clientDao.getAll();
        korisnici.forEach((c) -> {
            ShopItemGetTrendingIndexServiceTest.clientDao.deleteOne(c.getId());
        });
                
        dobavljaci = ShopItemGetTrendingIndexServiceTest.deliveryServiceDAO.getAll();
        dobavljaci.forEach((d) -> {
            ShopItemGetTrendingIndexServiceTest.deliveryServiceDAO.deleteOne(d.getId());
        });
        
        ShopItemGetTrendingIndexServiceTest.shopItemDAO = null;
        ShopItemGetTrendingIndexServiceTest.clientDao = null;
        ShopItemGetTrendingIndexServiceTest.transactionDao = null;
        ShopItemGetTrendingIndexServiceTest.deliveryServiceDAO = null;
        ShopItemGetTrendingIndexServiceTest.instance = null;
        
        
        proizvodi = null;
        korisnici = null;
        transakcije = null;    
        dobavljaci = null;
    }

    /**
     * Test of getTrendingIndex method, of class ShopItemService.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testGetTrendingIndex() {
        System.out.println("Nepostojeci id, ocekujemo IllegalArgumentException");
        ShopItem s = new ShopItem();
        float expResult = 0.0F;
        float result = instance.getTrendingIndex(s);
        assertEquals(expResult, result, 0.01);

    }
    
    @Test
    public void testGetTrendingIndex1() {
        System.out.println("Trazenje indexa bez transakcije za dati proizvod");
        ShopItem s = proizvodi.get(0);
        float expResult = 0.0F;
        float result = instance.getTrendingIndex(s);
        assertEquals(expResult, result, 0.01);

    }
    
    @Test
    public void testGetTrendingIndex2() {
        System.out.println("Trazenje indexa sa transakcijom pre 2 dana");
        ShopItem s = proizvodi.get(0);
        LocalDate localDate = LocalDate.now().minusDays(2);
        Date date = Date.valueOf(localDate);
        Transaction t = new Transaction(null, 120.0F, 10, date, korisnici.get(0).getId(), s.getId(), dobavljaci.get(0).getId(), 10);
        ShopItemGetTrendingIndexServiceTest.transactionDao.insertOne(t);
        float expResult = t.getTotalPrice()/2;
        float result = instance.getTrendingIndex(s);
        assertEquals(expResult, result, 0.01);

    }
    
    @Test
    public void testGetTrendingIndex3() {
        System.out.println("Trazenje indexa sa 2 transakcije, poslednja pre 6 dana");
        ShopItem s = proizvodi.get(0);
        LocalDate localDate = LocalDate.now().minusDays(2);
        Date date = Date.valueOf(localDate);
        Transaction t = new Transaction(null, 120.0F, 10, date, korisnici.get(0).getId(), s.getId(), dobavljaci.get(0).getId(), 10);
        ShopItemGetTrendingIndexServiceTest.transactionDao.insertOne(t);
        Transaction t2 = new Transaction(null, 12.0F, 1, date, korisnici.get(0).getId(), s.getId(), dobavljaci.get(0).getId(), 10);
        ShopItemGetTrendingIndexServiceTest.transactionDao.insertOne(t2);
        float expResult = (t.getTotalPrice() + t2.getTotalPrice())/2;
        float result = instance.getTrendingIndex(s);
        assertEquals(expResult, result, 0.01);

    }
    
    @Test
    public void testGetTrendingIndex4() {
        System.out.println("Trazenje indexa sa transakcijom na danasnji dan");
        ShopItem s = proizvodi.get(0);
        LocalDate localDate = LocalDate.now();
        Date date = Date.valueOf(localDate);
        Transaction t = new Transaction(null, 120.0F, 10, date, korisnici.get(0).getId(), s.getId(), dobavljaci.get(0).getId(), 10);
        ShopItemGetTrendingIndexServiceTest.transactionDao.insertOne(t);
        float expResult = 0.0F;
        float result = instance.getTrendingIndex(s);
        assertEquals(expResult, result, 0.01);

    }
    
}
