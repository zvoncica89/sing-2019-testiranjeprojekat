/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.ShopItem;

import db.ClientDAO;
import db.DatabaseConnection;
import db.DeliveryServiceDAO;
import db.ShopItemDAO;
import db.TransactionDAO;
import java.time.LocalDate;
import java.util.ArrayList;
import java.sql.Date;
import java.time.LocalDate;
import model.Client;
import model.DeliveryService;
import model.ShopItem;
import model.Transaction;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import service.ShopItemService;
import static org.junit.Assert.*;

/**
 *
 * @author minag
 */
public class ShopItemCheckIfPopularServiceTest {
    private static ShopItemDAO shopItemDAO;
    private static ShopItemService instance;
    private static ClientDAO clientDao;
    private static TransactionDAO transactionDao;
    private static DeliveryServiceDAO deliveryServiceDAO;
    ArrayList<ShopItem> proizvodi;
    ArrayList<Client> korisnici;
    ArrayList<Transaction> transakcije;
    ArrayList<DeliveryService> dobavljaci;
    
    public ShopItemCheckIfPopularServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        DatabaseConnection.getConnection();
        ShopItemCheckIfPopularServiceTest.shopItemDAO = new ShopItemDAO();
        ShopItemCheckIfPopularServiceTest.clientDao = new ClientDAO();
        ShopItemCheckIfPopularServiceTest.transactionDao = new TransactionDAO();
        ShopItemCheckIfPopularServiceTest.deliveryServiceDAO = new DeliveryServiceDAO();
        ShopItemCheckIfPopularServiceTest.instance = new ShopItemService();
        
        ShopItem proizvod1 = new ShopItem(null, "Bananica", 12.0F, 150);
        boolean expResult = true;
        boolean result = ShopItemCheckIfPopularServiceTest.shopItemDAO.insertOne(proizvod1);
        
        ShopItem proizvod2 = new ShopItem(null, "Merlot", 400.0F, 10);
        expResult = true;
        result = ShopItemCheckIfPopularServiceTest.shopItemDAO.insertOne(proizvod2);
        
        ShopItem proizvod3 = new ShopItem(null, "Chardonay", 300.0F, 10);
        expResult = true;
        result = ShopItemCheckIfPopularServiceTest.shopItemDAO.insertOne(proizvod3);
        
        proizvodi = ShopItemCheckIfPopularServiceTest.shopItemDAO.getAll();
        
        Client korisnik = new Client(null, "Mina", "mina", "mina");
        expResult = true;
        result = ShopItemCheckIfPopularServiceTest.clientDao.insertOne(korisnik);
        korisnici = ShopItemCheckIfPopularServiceTest.clientDao.getAll();
        
        DeliveryService dobavljac = new DeliveryService(null, "Stark", 10, 10);
        expResult = true;
        result = ShopItemCheckIfPopularServiceTest.deliveryServiceDAO.insertOne(dobavljac);
        dobavljaci = ShopItemCheckIfPopularServiceTest.deliveryServiceDAO.getAll();
        
        transakcije = new ArrayList<>();
        
    }
    
    @After
    public void tearDown() {
        DatabaseConnection.close();   
        transakcije = ShopItemCheckIfPopularServiceTest.transactionDao.getAll();
        transakcije.forEach((t) -> {
            ShopItemCheckIfPopularServiceTest.transactionDao.deleteOne(t.getId());
        });
        proizvodi = ShopItemCheckIfPopularServiceTest.shopItemDAO.getAll();
        proizvodi.forEach((d) -> {
            ShopItemCheckIfPopularServiceTest.shopItemDAO.deleteOne(d.getId());
        });
        korisnici = ShopItemCheckIfPopularServiceTest.clientDao.getAll();
        korisnici.forEach((c) -> {
            ShopItemCheckIfPopularServiceTest.clientDao.deleteOne(c.getId());
        });
                
        dobavljaci = ShopItemCheckIfPopularServiceTest.deliveryServiceDAO.getAll();
        dobavljaci.forEach((d) -> {
            ShopItemCheckIfPopularServiceTest.deliveryServiceDAO.deleteOne(d.getId());
        });
        
        ShopItemCheckIfPopularServiceTest.shopItemDAO = null;
        ShopItemCheckIfPopularServiceTest.clientDao = null;
        ShopItemCheckIfPopularServiceTest.transactionDao = null;
        ShopItemCheckIfPopularServiceTest.deliveryServiceDAO = null;
        ShopItemCheckIfPopularServiceTest.instance = null;
        
        
        proizvodi = null;
        korisnici = null;
        transakcije = null;    
        dobavljaci = null;
    }


    /**
     * Test of checkIfPopular method, of class ShopItemService.
     */
    @Test
    public void testCheckIfPopular() {
        System.out.println("CheckIfPopular cena < 300, kolicina < 80% lagera");
        ShopItem s = proizvodi.get(0);
        System.out.println();
        LocalDate localDate = LocalDate.now().minusDays(30);
        Date date = Date.valueOf(localDate);
        Transaction t = new Transaction(null, 120.0F, 10, date, korisnici.get(0).getId(), s.getId(), dobavljaci.get(0).getId(), 10);
        ShopItemCheckIfPopularServiceTest.transactionDao.insertOne(t);        
        boolean expResult = false;
        boolean result = instance.checkIfPopular(s);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCheckIfPopular2() {
        System.out.println("CheckIfPopular cena < 300, kolicina = 80% lagera");
        ShopItem s = proizvodi.get(0);
        System.out.println();
        LocalDate localDate = LocalDate.now().minusDays(30);
        Date date = Date.valueOf(localDate);
        Transaction t = new Transaction(null, 1440.0F, 120, date, korisnici.get(0).getId(), s.getId(), dobavljaci.get(0).getId(), 10);
        ShopItemCheckIfPopularServiceTest.transactionDao.insertOne(t);        
        boolean expResult = false;
        boolean result = instance.checkIfPopular(s);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCheckIfPopular3() {
        System.out.println("CheckIfPopular cena < 300, kolicina > 80% lagera");
        ShopItem s = proizvodi.get(0);
        System.out.println();
        LocalDate localDate = LocalDate.now().minusDays(30);
        Date date = Date.valueOf(localDate);
        Transaction t = new Transaction(null, 1560.0F, 130, date, korisnici.get(0).getId(), s.getId(), dobavljaci.get(0).getId(), 10);
        ShopItemCheckIfPopularServiceTest.transactionDao.insertOne(t);        
        boolean expResult = true;
        boolean result = instance.checkIfPopular(s);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCheckIfPopular4() {
        System.out.println("CheckIfPopular cena > 300, kolicina < 60% lagera");
        ShopItem s = proizvodi.get(1);
        System.out.println();
        LocalDate localDate = LocalDate.now().minusDays(30);
        Date date = Date.valueOf(localDate);
        Transaction t = new Transaction(null, 400.0F, 1, date, korisnici.get(0).getId(), s.getId(), dobavljaci.get(0).getId(), 10);
        ShopItemCheckIfPopularServiceTest.transactionDao.insertOne(t);        
        boolean expResult = false;
        boolean result = instance.checkIfPopular(s);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCheckIfPopular5() {
        System.out.println("CheckIfPopular cena > 300, kolicina = 60% lagera");
        ShopItem s = proizvodi.get(1);
        System.out.println();
        LocalDate localDate = LocalDate.now().minusDays(30);
        Date date = Date.valueOf(localDate);
        Transaction t = new Transaction(null, 2400.0F, 6, date, korisnici.get(0).getId(), s.getId(), dobavljaci.get(0).getId(), 10);
        ShopItemCheckIfPopularServiceTest.transactionDao.insertOne(t);        
        boolean expResult = false;
        boolean result = instance.checkIfPopular(s);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCheckIfPopular6() {
        System.out.println("CheckIfPopular cena > 300, kolicina > 60% lagera");
        ShopItem s = proizvodi.get(1);
        System.out.println();
        LocalDate localDate = LocalDate.now().minusDays(20);
        Date date = Date.valueOf(localDate);
        Transaction t = new Transaction(null, 4000.0F, 10, date, korisnici.get(0).getId(), s.getId(), dobavljaci.get(0).getId(), 10);
        ShopItemCheckIfPopularServiceTest.transactionDao.insertOne(t);        
        boolean expResult = true;
        boolean result = instance.checkIfPopular(s);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCheckIfPopular7() {
        System.out.println("CheckIfPopular cena = 300, kolicina nebitna");
        ShopItem s = proizvodi.get(2);
        System.out.println();
        LocalDate localDate = LocalDate.now().minusDays(20);
        Date date = Date.valueOf(localDate);
        Transaction t = new Transaction(null, 300.0F, 1, date, korisnici.get(0).getId(), s.getId(), dobavljaci.get(0).getId(), 10);
        ShopItemCheckIfPopularServiceTest.transactionDao.insertOne(t);        
        boolean expResult = false;
        boolean result = instance.checkIfPopular(s);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCheckIfPopular8() {
        System.out.println("CheckIfPopular cena > 300, kolicina < 60% nema transakcije");
        ShopItem s = proizvodi.get(2);
        System.out.println();
        LocalDate localDate = LocalDate.now().minusDays(20);
        Date date = Date.valueOf(localDate);     
        boolean expResult = false;
        boolean result = instance.checkIfPopular(s);
        assertEquals(expResult, result);
    }
}
