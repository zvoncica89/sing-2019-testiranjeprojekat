/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.ShopItem;

import db.DatabaseConnection;
import db.ShopItemDAO;
import java.util.ArrayList;
import model.ShopItem;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import service.ShopItemService;
import static org.junit.Assert.*;

/**
 *
 * @author minag
 */
public class ShopItemRemoveItemServiceTest {
    private static ShopItemDAO shopItemDAO;
    private static ShopItemService instance;
    ArrayList<ShopItem> proizvodi;
    
    public ShopItemRemoveItemServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        DatabaseConnection.getConnection();
        ShopItemRemoveItemServiceTest.shopItemDAO = new ShopItemDAO();
        ShopItemRemoveItemServiceTest.instance = new ShopItemService();
        
        ShopItem proizvod1 = new ShopItem(null, "Bananica", 12.0F, 150);
        boolean expResult = true;
        boolean result = ShopItemRemoveItemServiceTest.shopItemDAO.insertOne(proizvod1);
        
        ShopItem proizvod2 = new ShopItem(null, "Coca-Cola", 120.0F, 10);
        expResult = true;
        result = ShopItemRemoveItemServiceTest.shopItemDAO.insertOne(proizvod2);
        
        proizvodi = ShopItemRemoveItemServiceTest.shopItemDAO.getAll();
    }
    
    @After
    public void tearDown() {
        DatabaseConnection.close();     
        proizvodi.forEach((d) -> {
            ShopItemRemoveItemServiceTest.shopItemDAO.deleteOne(d.getId());
        });
        ShopItemRemoveItemServiceTest.shopItemDAO = null;
        ShopItemRemoveItemServiceTest.instance = null;
        proizvodi = null;
    }

    /**
     * Test of postItem method, of class ShopItemService.
     */

    /**
     * Test of removeItem method, of class ShopItemService.
     */
    @Test
    public void testRemoveItem() {
        System.out.println("RemoveItem sa nepostojecim proizvodom");
        ShopItem s = new ShopItem();
        boolean expResult = false;
        boolean result = instance.removeItem(s);
        assertEquals(expResult, result);
    }
    
    public void testRemoveItem2() {
        System.out.println("RemoveItem postojeceg proizvoda");
        ShopItem s = proizvodi.get(0);
        boolean expResult = true;
        boolean result = instance.removeItem(s);
        assertEquals(expResult, result);
    }
    
    public void testRemoveItem3() {
        System.out.println("Provera da en postoji obrisani proizvod");
        ShopItem s = proizvodi.get(0);
        instance.removeItem(s);
        proizvodi = ShopItemRemoveItemServiceTest.shopItemDAO.getAll();
        for (ShopItem item : proizvodi){
            assertNotEquals(s.getId(), item.getId());
        }
    }
    
}
