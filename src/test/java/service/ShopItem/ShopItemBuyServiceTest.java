/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.ShopItem;

import db.DatabaseConnection;
import db.ShopItemDAO;
import java.util.ArrayList;
import model.ShopItem;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import service.ShopItemService;
import static org.junit.Assert.*;

/**
 *
 * @author minag
 */
public class ShopItemBuyServiceTest {
    private static ShopItemDAO shopItemDAO;
    private static ShopItemService instance;
    ArrayList<ShopItem> proizvodi;
    
    public ShopItemBuyServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        DatabaseConnection.getConnection();
        ShopItemBuyServiceTest.shopItemDAO = new ShopItemDAO();
        ShopItemBuyServiceTest.instance = new ShopItemService();
        
        ShopItem proizvod1 = new ShopItem(null, "Bananica", 12.0F, 150);
        boolean expResult = true;
        boolean result = ShopItemBuyServiceTest.shopItemDAO.insertOne(proizvod1);
        
        ShopItem proizvod2 = new ShopItem(null, "Coca-Cola", 120.0F, 10);
        expResult = true;
        result = ShopItemBuyServiceTest.shopItemDAO.insertOne(proizvod2);
        
        proizvodi = ShopItemBuyServiceTest.shopItemDAO.getAll();
    }
    
    @After
    public void tearDown() {
        DatabaseConnection.close();     
        proizvodi.forEach((d) -> {
            ShopItemBuyServiceTest.shopItemDAO.deleteOne(d.getId());
        });
        ShopItemBuyServiceTest.shopItemDAO = null;
        ShopItemBuyServiceTest.instance = null;
        proizvodi = null;
    }
 

    /**
     * Test of buy method, of class ShopItemService.
     */
    @Test(expected = NullPointerException.class)
    public void testBuy1() {
        System.out.println("Kupovina proizvoda koji ne postoji");
        ShopItem s = new ShopItem();
        int amount = 0;
        instance.buy(s, amount);        
    }
    
    @Test
    public void testBuy2() {
        System.out.println("Kupovina proizvoda sa kolicinom < 0");
        ShopItem s = proizvodi.get(1);
        int amount = -10;
        int expResult = s.getAmount();
        instance.buy(s, amount);
        ShopItem proizvod = ShopItemBuyServiceTest.shopItemDAO.getOne(s.getId());
        assertEquals(expResult, proizvod.getAmount());
    }
    
    @Test
    public void testBuy3() {
        System.out.println("Kupovina proizvoda sa kolicinom = 0");
        ShopItem s = proizvodi.get(0);
        int amount = 0;
        instance.buy(s, amount);
        ShopItem proizvod = ShopItemBuyServiceTest.shopItemDAO.getOne(s.getId());
        assertEquals(s.getAmount(), proizvod.getAmount());
    }
    
    @Test
    public void testBuy4() {
        System.out.println("Kupovina proizvoda sa kolicinom > 0, amount > s.getAmount() posle setovanja, ali ne vecom od kolicine na stanju");
        ShopItem s = proizvodi.get(1);
        int amount = 7;
        int expResult = s.getAmount()-7;
        instance.buy(s, amount);
        ShopItem proizvod = ShopItemBuyServiceTest.shopItemDAO.getOne(s.getId());
        assertEquals(expResult, proizvod.getAmount());
    }
    @Test
    public void testBuy41() {
        System.out.println("Kupovina proizvoda sa kolicinom > 0, amount = s.getAmount() posle setovanja, ali ne vecom od kolicine na stanju");
        ShopItem s = proizvodi.get(1);
        int amount = 5;
        int expResult = s.getAmount()-5;
        instance.buy(s, amount);
        ShopItem proizvod = ShopItemBuyServiceTest.shopItemDAO.getOne(s.getId());
        assertEquals(expResult, proizvod.getAmount());
    }
    
    @Test
    public void testBuy42() {
        System.out.println("Kupovina proizvoda sa kolicinom > 0, amount < s.getAmount() posle setovanja, ali ne vecom od kolicine na stanju");
        ShopItem s = proizvodi.get(1);
        int amount = 3;
        int expResult = s.getAmount()-3;
        instance.buy(s, amount);
        ShopItem proizvod = ShopItemBuyServiceTest.shopItemDAO.getOne(s.getId());
        assertEquals(expResult, proizvod.getAmount());
    }
    
    
    
    @Test
    public void testBuy5() {
        System.out.println("Kupovina proizvoda sa kolicinom > 0, ali vecom od kolicine na stanju");
        ShopItem s = proizvodi.get(1);
        int amount = 15;
        int expResult = s.getAmount();
        instance.buy(s, amount);
        ShopItem proizvod = ShopItemBuyServiceTest.shopItemDAO.getOne(s.getId());
        assertEquals(expResult, proizvod.getAmount());
    }
    
}
