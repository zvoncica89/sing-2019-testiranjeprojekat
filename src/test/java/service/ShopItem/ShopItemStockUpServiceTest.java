/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.ShopItem;

import db.DatabaseConnection;
import db.ShopItemDAO;
import java.util.ArrayList;
import model.ShopItem;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import service.ShopItemService;
import static org.junit.Assert.*;

/**
 *
 * @author minag
 */
public class ShopItemStockUpServiceTest {
    private static ShopItemDAO shopItemDAO;
    private static ShopItemService instance;
    ArrayList<ShopItem> proizvodi;
    
    public ShopItemStockUpServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        DatabaseConnection.getConnection();
        ShopItemStockUpServiceTest.shopItemDAO = new ShopItemDAO();
        ShopItemStockUpServiceTest.instance = new ShopItemService();
        
        ShopItem proizvod1 = new ShopItem(null, "Bananica", 12.0F, 150);
        boolean expResult = true;
        boolean result = ShopItemStockUpServiceTest.shopItemDAO.insertOne(proizvod1);
        
        ShopItem proizvod2 = new ShopItem(null, "Coca-Cola", 120.0F, 10);
        expResult = true;
        result = ShopItemStockUpServiceTest.shopItemDAO.insertOne(proizvod2);
        
        proizvodi = ShopItemStockUpServiceTest.shopItemDAO.getAll();
    }
    
    @After
    public void tearDown() {
        DatabaseConnection.close();     
        proizvodi.forEach((d) -> {
            ShopItemStockUpServiceTest.shopItemDAO.deleteOne(d.getId());
        });
        ShopItemStockUpServiceTest.shopItemDAO = null;
        ShopItemStockUpServiceTest.instance = null;
        proizvodi = null;
    }


    /**
     * Test of stockUp method, of class ShopItemService.
     */
    @Test(expected = NullPointerException.class)
    public void testStockUp() {
        System.out.println("StockUp za nepostojeci proizvod");
        ShopItem s = new ShopItem();
        int amount = 0;
        ShopItemService instance = new ShopItemService();
        instance.stockUp(s, amount);
    }
    
    @Test
    public void testStockUp2() {
        System.out.println("StockUp za amount < 0");
        ShopItem s = proizvodi.get(0);
        int amount = -5;
        int expResult = s.getAmount();
        ShopItemService instance = new ShopItemService();
        instance.stockUp(s, amount);
        ShopItem proizvod = ShopItemStockUpServiceTest.shopItemDAO.getOne(s.getId());
        assertEquals(expResult, proizvod.getAmount());
        
    }
    
    @Test
    public void testStockUp3() {
        System.out.println("StockUp za amount = 0");
        ShopItem s = proizvodi.get(0);
        int amount = 0;
        int expResult = s.getAmount();
        ShopItemService instance = new ShopItemService();
        instance.stockUp(s, amount);
        ShopItem proizvod = ShopItemStockUpServiceTest.shopItemDAO.getOne(s.getId());
        assertEquals(expResult, proizvod.getAmount());
        
    }
    
    @Test
    public void testStockUp4() {
        System.out.println("StockUp za amount > 0");
        ShopItem s = proizvodi.get(0);
        int amount = 100;
        int expResult = s.getAmount()+100;
        ShopItemService instance = new ShopItemService();
        instance.stockUp(s, amount);
        ShopItem proizvod = ShopItemStockUpServiceTest.shopItemDAO.getOne(s.getId());
        assertEquals(expResult, proizvod.getAmount());
        
    }
    
    
    
}
