/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.DeliveryService;

import db.DatabaseConnection;
import db.DeliveryServiceDAO;
import java.util.ArrayList;
import model.Client;
import model.DeliveryService;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import service.DeliveryServiceService;
import static org.junit.Assert.*;
import service.Client.ClientServiceUpdateInfoTest;

/**
 *
 * @author minag
 */
public class DeliveryServiceRegisterServiceTest {
    private static DeliveryServiceDAO deliveryServiceDAO;
    private static DeliveryServiceService instance;
    ArrayList<DeliveryService> dobavljaci;
    
    public DeliveryServiceRegisterServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        DatabaseConnection.getConnection();
        DeliveryServiceRegisterServiceTest.deliveryServiceDAO = new DeliveryServiceDAO();
        DeliveryServiceRegisterServiceTest.instance = new DeliveryServiceService();
        
        DeliveryService dobavljac1 = new DeliveryService(null, "Stark", 10, 10);
        boolean expResult = true;
        boolean result = DeliveryServiceRegisterServiceTest.deliveryServiceDAO.insertOne(dobavljac1);
        assertEquals(expResult, result);
        
        DeliveryService dobavljac2 = new DeliveryService(null, "Pionir", 5, 5);
        expResult = true;
        result = DeliveryServiceRegisterServiceTest.deliveryServiceDAO.insertOne(dobavljac2);
        assertEquals(expResult, result);
        
        dobavljaci = DeliveryServiceRegisterServiceTest.deliveryServiceDAO.getAll();
    }
    
    @After
    public void tearDown() {
        DatabaseConnection.close();     
        dobavljaci.forEach((d) -> {
            DeliveryServiceRegisterServiceTest.deliveryServiceDAO.deleteOne(d.getId());
        });
        DeliveryServiceRegisterServiceTest.deliveryServiceDAO = null;
        DeliveryServiceRegisterServiceTest.instance = null;
        dobavljaci = null;
    }

    /**
     * Test of register method, of class DeliveryServiceService.
     */
    
    /** pricePerKilometer:
     * legalna klasa: pricePerKilometer > 0;
     * nelegalna klasa: pricePerKilometer < 0;
     * 
     * startingPrice:
     * legalna klasa: startingPrice > 0;
     * nelegalna klasa: startingPrice < 0;
    */
    @Test
    public void testRegister1() {
        System.out.println("Registracija sa praznim imenom, cenom < 0, cena po kilometru < 0");
        String name = "";
        float pricePerKilometer = -3.0F;
        float startingPrice = -30.0F;
        boolean expResult = false;
        boolean result = instance.register(name, pricePerKilometer, startingPrice);
        assertEquals(expResult, result);
    }
    @Test
    public void testRegister12() {
        System.out.println("Registracija sa praznim imenom, cenom < 0, cena po kilometru = 0");
        String name = "";
        float pricePerKilometer = -3.0F;
        float startingPrice = 0.0F;
        boolean expResult = false;
        boolean result = instance.register(name, pricePerKilometer, startingPrice);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testRegister13() {
        System.out.println("Registracija sa praznim imenom, cenom < 0, cena po kilometru > 0");
        String name = "";
        float pricePerKilometer = -3.0F;
        float startingPrice = 15.0F;
        boolean expResult = false;
        boolean result = instance.register(name, pricePerKilometer, startingPrice);
        assertEquals(expResult, result);
    }
    @Test
    public void testRegister14() {
        System.out.println("Registracija sa praznim imenom, cenom = 0, cena po kilometru < 0");
        String name = "";
        float pricePerKilometer = 0.0F;
        float startingPrice = -30.0F;
        boolean expResult = false;
        boolean result = instance.register(name, pricePerKilometer, startingPrice);
        assertEquals(expResult, result);
    }
    @Test
    public void testRegister15() {
        System.out.println("Registracija sa praznim imenom, cenom > 0, cena po kilometru < 0");
        String name = "";
        float pricePerKilometer = 15.0F;
        float startingPrice = -30.0F;
        boolean expResult = false;
        boolean result = instance.register(name, pricePerKilometer, startingPrice);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testRegister2() {
        System.out.println("Registracija sa praznim imenom, cenom = 0, cena po kilometru = 0");
        String name = "";
        float pricePerKilometer = 0.0F;
        float startingPrice = 0.0F;
        boolean expResult = false;
        boolean result = instance.register(name, pricePerKilometer, startingPrice);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testRegister3() {
        System.out.println("Registracija sa novim imenom, cenom = 0, cena po kilometru = 0");
        String name = "Novo ime";
        float pricePerKilometer = 0.0F;
        float startingPrice = 0.0F;
        boolean expResult = false;
        boolean result = instance.register(name, pricePerKilometer, startingPrice);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testRegister4() {
        System.out.println("Registracija sa postojecim imenom, cenom = 0, cena po kilometru = 0");
        String name = "Stark";
        float pricePerKilometer = 0.0F;
        float startingPrice = 0.0F;
        boolean expResult = false;
        boolean result = instance.register(name, pricePerKilometer, startingPrice);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testRegister5() {
        System.out.println("Registracija sa novim imenom, cenom > 0, cena po kilometru = 0");
        String name = "Novo ime";
        float pricePerKilometer = 15.0F;
        float startingPrice = 0.0F;
        boolean expResult = false;
        boolean result = instance.register(name, pricePerKilometer, startingPrice);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testRegister6() {
        System.out.println("Registracija sa novim imenom, cenom = 0, cena po kilometru > 0");
        String name = "Novo ime";
        float pricePerKilometer = 0.0F;
        float startingPrice = 15.0F;
        boolean expResult = false;
        boolean result = instance.register(name, pricePerKilometer, startingPrice);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testRegister7() {
        System.out.println("Registracija sa novim imenom, cenom > 0, cena po kilometru > 0");
        String name = "Novo ime";
        float pricePerKilometer = 15.0F;
        float startingPrice = 15.0F;
        boolean expResult = true;
        boolean result = instance.register(name, pricePerKilometer, startingPrice);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testRegister8() {
        System.out.println("Registracija sa novim imenom, cenom > 0, cena po kilometru > 0, sa proverom unosa u bazu");
        String name = "Novo ime";
        float pricePerKilometer = 15.0F;
        float startingPrice = 15.0F;
        instance.register(name, pricePerKilometer, startingPrice);
        int expResult = dobavljaci.size()+1;
        dobavljaci = DeliveryServiceRegisterServiceTest.deliveryServiceDAO.getAll();        
        assertEquals(expResult, dobavljaci.size());
    }
    
    @Test
    public void testRegister9() {
        System.out.println("Registracija sa istim imenom, cenom > 0, cena po kilometru > 0, sa proverom da li u bazi postoje dva sa istim imenom, ali razlicitim id-em");
        String name = "Stark";
        float pricePerKilometer = 15.0F;
        float startingPrice = 15.0F;
        instance.register(name, pricePerKilometer, startingPrice);
        dobavljaci = DeliveryServiceRegisterServiceTest.deliveryServiceDAO.getAll();   
        ArrayList<DeliveryService> istiDobavljaci = new ArrayList<>();
        for (DeliveryService d : dobavljaci){
            if (d.getName().equals(name)){
                istiDobavljaci.add(d);
            }
        }
        assertNotEquals(istiDobavljaci.get(0).getId(), istiDobavljaci.get(1).getId());
        
    }

    
    
}
