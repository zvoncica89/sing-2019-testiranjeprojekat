/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.DeliveryService;

import db.DatabaseConnection;
import db.DeliveryServiceDAO;
import java.util.ArrayList;
import model.DeliveryService;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import service.DeliveryServiceService;
import static org.junit.Assert.*;

/**
 *
 * @author minag
 */
public class DeliveryServiceDeleteDeliveryServiceServiceTest {
    private static DeliveryServiceDAO deliveryServiceDAO;
    private static DeliveryServiceService instance;
    ArrayList<DeliveryService> dobavljaci;
    
    public DeliveryServiceDeleteDeliveryServiceServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        DatabaseConnection.getConnection();
        DeliveryServiceDeleteDeliveryServiceServiceTest.deliveryServiceDAO = new DeliveryServiceDAO();
        DeliveryServiceDeleteDeliveryServiceServiceTest.instance = new DeliveryServiceService();
        
        DeliveryService dobavljac1 = new DeliveryService(null, "Stark", 10, 10);
        boolean expResult = true;
        boolean result = DeliveryServiceDeleteDeliveryServiceServiceTest.deliveryServiceDAO.insertOne(dobavljac1);
        assertEquals(expResult, result);
        
        DeliveryService dobavljac2 = new DeliveryService(null, "Pionir", 5, 5);
        expResult = true;
        result = DeliveryServiceDeleteDeliveryServiceServiceTest.deliveryServiceDAO.insertOne(dobavljac2);
        assertEquals(expResult, result);
        
        dobavljaci = DeliveryServiceDeleteDeliveryServiceServiceTest.deliveryServiceDAO.getAll();
    }
    
    @After
    public void tearDown() {
        DatabaseConnection.close();     
        dobavljaci.forEach((d) -> {
            DeliveryServiceDeleteDeliveryServiceServiceTest.deliveryServiceDAO.deleteOne(d.getId());
        });
        DeliveryServiceDeleteDeliveryServiceServiceTest.deliveryServiceDAO = null;
        DeliveryServiceDeleteDeliveryServiceServiceTest.instance = null;
        dobavljaci = null;
    }


    /**
     * Test of deleteDeliveryService method, of class DeliveryServiceService.
     */
    @Test
    public void testDeleteDeliveryService() {
        System.out.println("Brisanje nepostojeceg dobavljaca");
        DeliveryService d = new DeliveryService(null, "Nesto", 10.0f, 10.0F);
        boolean expResult = false;
        boolean result = instance.deleteDeliveryService(d);
        assertEquals(expResult, result);
        
    }
    
    @Test
    public void testDeleteDeliveryService2() {
        System.out.println("Brisanje postojeceg dobavljaca");
        DeliveryService d = dobavljaci.get(0);
        boolean expResult = true;
        boolean result = instance.deleteDeliveryService(d);
        assertEquals(expResult, result);        
    }
    
    @Test
    public void testDeleteDeliveryService3() {
        System.out.println("Provera da li se brise bas trazeni dobavljac");
        DeliveryService d = dobavljaci.get(0);
        instance.deleteDeliveryService(d);
        dobavljaci = DeliveryServiceDeleteDeliveryServiceServiceTest.deliveryServiceDAO.getAll();
        for (DeliveryService dobavljac : dobavljaci){
            assertNotEquals(d.getId(), dobavljac.getId());
        }
    }

    
}
