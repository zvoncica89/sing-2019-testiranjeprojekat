/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.DeliveryService;

import db.DatabaseConnection;
import db.DeliveryServiceDAO;
import java.util.ArrayList;
import model.DeliveryService;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import service.DeliveryServiceService;
import static org.junit.Assert.*;

/**
 *
 * @author minag
 */
public class DeliveryServiceUpdateInfoServiceTest {
    private static DeliveryServiceDAO deliveryServiceDAO;
    private static DeliveryServiceService instance;
    ArrayList<DeliveryService> dobavljaci;
    
    public DeliveryServiceUpdateInfoServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        DatabaseConnection.getConnection();
        DeliveryServiceUpdateInfoServiceTest.deliveryServiceDAO = new DeliveryServiceDAO();
        DeliveryServiceUpdateInfoServiceTest.instance = new DeliveryServiceService();
        
        DeliveryService dobavljac1 = new DeliveryService(null, "Stark", 10, 10);
        boolean expResult = true;
        boolean result = DeliveryServiceUpdateInfoServiceTest.deliveryServiceDAO.insertOne(dobavljac1);
        assertEquals(expResult, result);
        
        DeliveryService dobavljac2 = new DeliveryService(null, "Pionir", 5, 5);
        expResult = true;
        result = DeliveryServiceUpdateInfoServiceTest.deliveryServiceDAO.insertOne(dobavljac2);
        assertEquals(expResult, result);
        
        dobavljaci = DeliveryServiceUpdateInfoServiceTest.deliveryServiceDAO.getAll();
    }
    
    @After
    public void tearDown() {
        DatabaseConnection.close();     
        dobavljaci.forEach((d) -> {
            DeliveryServiceUpdateInfoServiceTest.deliveryServiceDAO.deleteOne(d.getId());
        });
        DeliveryServiceUpdateInfoServiceTest.deliveryServiceDAO = null;
        DeliveryServiceUpdateInfoServiceTest.instance = null;
        dobavljaci = null;
    }


    /**
     * Test of updateInfo method, of class DeliveryServiceService.
     */
    
    /** pricePerKilometer:
     * legalna klasa: pricePerKilometer > 0;
     * nelegalna klasa: pricePerKilometer < 0;
     * 
     * startingPrice:
     * legalna klasa: startingPrice > 0;
     * nelegalna klasa: startingPrice < 0;
    */
    
    @Test
    public void testUpdateInfo() {
        System.out.println("UpdateInfo sa praznim objektom");
        DeliveryService d = null;
        String name = null;
        float startingPrice = 0.0F;
        float pricePerKilometer = 0.0F;
        boolean expResult = false;
        boolean result = instance.updateInfo(d, name, startingPrice, pricePerKilometer);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testUpdateInfo2() {
        System.out.println("UpdateInfo sa novim imenom, cenom < 0, i cenom po kilometru < 0");
        DeliveryService d = dobavljaci.get(0);
        String name = "Novo ime";
        float startingPrice = -15.0F;
        float pricePerKilometer = -15.0F;
        boolean expResult = false;
        boolean result = instance.updateInfo(d, name, startingPrice, pricePerKilometer);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testUpdateInfo21() {
        System.out.println("UpdateInfo sa novim imenom, cenom < 0, i cenom po kilometru = 0");
        DeliveryService d = dobavljaci.get(0);
        String name = "Novo ime";
        float startingPrice = -15.0F;
        float pricePerKilometer = 0.0F;
        boolean expResult = false;
        boolean result = instance.updateInfo(d, name, startingPrice, pricePerKilometer);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testUpdateInfo22() {
        System.out.println("UpdateInfo sa novim imenom, cenom < 0, i cenom po kilometru > 0");
        DeliveryService d = dobavljaci.get(0);
        String name = "Novo ime";
        float startingPrice = -15.0F;
        float pricePerKilometer = 10.0F;
        boolean expResult = false;
        boolean result = instance.updateInfo(d, name, startingPrice, pricePerKilometer);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testUpdateInfo23() {
        System.out.println("UpdateInfo sa novim imenom, cenom = 0, i cenom po kilometru < 0");
        DeliveryService d = dobavljaci.get(0);
        String name = "Novo ime";
        float startingPrice = 0.0F;
        float pricePerKilometer = -10.0F;
        boolean expResult = false;
        boolean result = instance.updateInfo(d, name, startingPrice, pricePerKilometer);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testUpdateInfo24() {
        System.out.println("UpdateInfo sa novim imenom, cenom > 0, i cenom po kilometru < 0");
        DeliveryService d = dobavljaci.get(0);
        String name = "Novo ime";
        float startingPrice = 10.0F;
        float pricePerKilometer = -10.0F;
        boolean expResult = false;
        boolean result = instance.updateInfo(d, name, startingPrice, pricePerKilometer);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testUpdateInfo3() {
        System.out.println("UpdateInfo sa novim imenom, cenom = 0, i cenom po kilometru = 0");
        DeliveryService d = dobavljaci.get(0);
        String name = "Novo ime";
        float startingPrice = 0.0F;
        float pricePerKilometer = 0.0F;
        boolean expResult = false;
        boolean result = instance.updateInfo(d, name, startingPrice, pricePerKilometer);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testUpdateInfo4() {
        System.out.println("UpdateInfo sa novim imenom, cenom > 0, i cenom po kilometru > 0");
        DeliveryService d = dobavljaci.get(0);
        String name = "Novo ime";
        float startingPrice = 10.0F;
        float pricePerKilometer = 10.0F;
        boolean expResult = true;
        boolean result = instance.updateInfo(d, name, startingPrice, pricePerKilometer);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testUpdateInfo41() {
        System.out.println("UpdateInfo sa novim imenom, istom cenom, i istom cenom po kilometru");
        DeliveryService d = dobavljaci.get(0);
        String name = "Novo ime";
        float startingPrice = d.getStartingPrice();
        float pricePerKilometer = d.getPricePerKilometer();
        boolean expResult = true;
        boolean result = instance.updateInfo(d, name, startingPrice, pricePerKilometer);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testUpdateInfo42() {
        System.out.println("UpdateInfo sa istim imenom, razlicitom cenom, i istom cenom po kilometru");
        DeliveryService d = dobavljaci.get(0);
        String name = d.getName();
        float startingPrice = 10.0F;
        float pricePerKilometer = d.getPricePerKilometer();
        boolean expResult = true;
        boolean result = instance.updateInfo(d, name, startingPrice, pricePerKilometer);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testUpdateInfo43() {
        System.out.println("UpdateInfo sa istim imenom, istom cenom, i razlicitom cenom po kilometru");
        DeliveryService d = dobavljaci.get(0);
        String name = d.getName();
        float startingPrice = d.getStartingPrice();
        float pricePerKilometer = 10.0F;
        boolean expResult = true;
        boolean result = instance.updateInfo(d, name, startingPrice, pricePerKilometer);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testUpdateInfo5() {
        System.out.println("Provera da li je update-ovan taj");
        DeliveryService d = dobavljaci.get(0);
        String name = "Novo ime";
        float startingPrice = 10.0F;
        float pricePerKilometer = 10.0F;
        instance.updateInfo(d, name, startingPrice, pricePerKilometer);
        dobavljaci = DeliveryServiceUpdateInfoServiceTest.deliveryServiceDAO.getAll();
        DeliveryService izmenjeniDobavljac = null;
        for (DeliveryService dobavljac : dobavljaci){
            if (dobavljac.getId().equals(d.getId())){
                izmenjeniDobavljac = dobavljac;
            }
        }
        assertEquals(name, izmenjeniDobavljac.getName());
        assertEquals(startingPrice, izmenjeniDobavljac.getStartingPrice(), 0.01);
        assertEquals(pricePerKilometer, izmenjeniDobavljac.getPricePerKilometer(), 0.01);
    }
}
